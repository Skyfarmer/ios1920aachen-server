# Server

A Qt application which displays and streams the footage of a connected microscope. 
It can also connect to, receive and display annotations from connected clients, such as the iPad ICARAS app in the microscope's eyepiece. 

## Dependencies

Run the script `install.sh` (Ubuntu).

## Building

* Run the `build.sh`script or execute commands manually:

	1. Create a directory which should contain the build files. Preferably named `cmake-build-debug` or `cmake-build-release` (respective to the cmake build type (debug/release)).
	2. Open the created directory in a CLI and execute `cmake .. -DCMAKE_BUILD_TYPE=DEBUG` or `cmake .. -DCMAKE_BUILD_TYPE=RELEASE`.
	3. Build the Makefile with `make`. This should create an executable called `multiviewer`. **Note:** You cannot use symbolic links when executing `make`.

## Start

Run `run.sh`. The script will search for `cmake-build-release/multiviewer`.
**Attention:** If the application itself *cannot* start an Avahi mDNS service and/or you decide to remove the Bonjour code you need to re-add the 5th line in the `run.sh` script, which publishes an avahi-service trough the shell script.

