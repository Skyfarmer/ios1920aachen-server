import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3

import Qt.labs.settings 1.0

import org.freedesktop.gstreamer.GLVideoItem 1.0

import de.aachen.utilities 1.0
import de.aachen.websocket 1.0
import de.aachen.multimedia 1.0

import "Components"
import "Windows"

ApplicationWindow {
    objectName: "UserWindow"
    id: rootWindow
    visible: true

    width: 1040
    height: 830
    minimumWidth: 640
    minimumHeight: 530
    title: "ICARAS"

    Component.onCompleted: {
        let screens = Qt.application.screens;
        let microscopeWindow = null;

        let mainScreen = null;
        for(let i = 0; i < screens.length; i++) {
            let screen = screens[i];

            console.log("Screen found: " + screen.name)

            if((screen.width == 2560 && screen.height == 1024)) {
                console.log("Microscope display found");

                microscopeWindow = openMicroscopeWindow(screen, true);

                continue;
            }

            mainScreen = screen;
        }

        // remove in production
        if(microscopeWindow === null) {
            microscopeWindow = openMicroscopeWindow(mainScreen, false);
        }

        x = mainScreen.virtualX;
        y = mainScreen.virtualY;

        if(microscopeWindow == null) {
            fatalError("No microscope found! Make sure the microscope is connected.");

            return;
        }

        pipeline.setQmlSink(videoMain, Pipeline.Main);
        pipeline.setQmlSink(microscopeWindow.videoLeft, Pipeline.Left);
        pipeline.setQmlSink(microscopeWindow.videoRight, Pipeline.Right);
    }

    onClosing: {
        if(!pipeline.isStopped) {
           close.accepted = false
           pipeline.stop()
        }
    }

    function fatalError(message) {
        let component = Qt.createComponent("Windows/Fatal.qml");

        component.createObject(rootWindow, {
            text: message,
        });
    }

    function openMicroscopeWindow(screen, fullScreen = true) {
        console.log("open microscope screen on " + screen.name);

        let component = Qt.createComponent("Windows/MicroscopeWindow.qml");

        let window = component.createObject(rootWindow, {
            screen: screen,
            x: screen.virtualX,
            y: screen.virtualY,
            //visibility: fullScreen ? "FullScreen" : ""
        });

        if(fullScreen) {
            window.showFullScreen();
        } else {
            window.show();
        }

        wsServer.registerSurface(window.annotationsLeft);
        wsServer.registerSurface(window.annotationsRight);

        return window;
    }

    function openSubWindow(name, width = null, height = null) {
        let component = Qt.createComponent("Windows/" + name + ".qml");

        let windowWidth = width ? Math.max(width, rootWindow.width * 0.8) : rootWindow.width * 0.8;
        let windowHeight = height ? Math.max(width, 500) : 500;

        let window = component.createObject(rootWindow, {
            x: rootWindow.x + rootWindow.width / 2 - windowWidth / 2,
            y: rootWindow.y + rootWindow.height / 2 - windowHeight / 2,
            transientParent: rootWindow.window,
            width: windowWidth,
            height: windowHeight,
        });

        return window;
    }

    WebSocketServer {
        id: wsServer

        property int clientsConnected: 0

        Component.onCompleted: {
            wsServer.registerSurface(annotationsUser);
            wsServer.setServerName(settings.microscopeName);
        }

        onClientConnected: {
            clientsConnected += 1;

            if(clientsConnected > 0) {
                pipeline.startStreaming(wsServer.getAddresses()[0], 8554);
            }
        }

        onClosed: {
            clientsConnected -= 1;

            if(clientsConnected == 0) {
                pipeline.stopStreaming();
            }
        }
    }

    Pipeline {
        id: pipeline
        property bool isStopped: true

        onStreamStarted: {
            wsServer.sendRtspUrl(rtsp);
        }

        onStopped: {
            isStopped = true;
            rootWindow.close();
        }
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&Icaras")

            /*Action  {
                text: "Connect to Microscope"
                onTriggered: {
                    pipeline.play();
                    pipeline.isStopped = false;
                }
            }*/

            Action  {
                text: "Calibrate Microscope"
            }

            MenuSeparator { }

            Action {
                text: qsTr("&Settings")
                onTriggered: rootWindow.openSubWindow("SettingWindow", 600, 600)
            }

            Action {
                text: qsTr("&Quit")
                onTriggered: rootWindow.close()
            }
        }

        Menu {
            title: qsTr("&Pairing")
            Action {
                text: qsTr("&QR &Code")
                onTriggered: {
                    let window = rootWindow.openSubWindow("QRCodeWindow");
                    window.address = wsServer.getAddresses()[0];
                }
            }
        }

        Menu {
            title: qsTr("&Help")

            Action {
                text: qsTr("&Documentation")
                onTriggered: Qt.openUrlExternally("file://" + appPath + "/Static/Documentation.pdf")
            }

            Action {
                text: qsTr("&About")
                onTriggered: rootWindow.openSubWindow("AboutWindow", 500, 700)
            }
        }
    }

    header: UserMenu {
        onRecordingChanged: {
            if(recording == true) {
                let date = new Date();

                pipeline.startRecording(settings.videoRecordingDestination.replace("file://", "") + "/" + settings.microscopeName + "-" + date.toISOString() + ".mov");
            } else {
                pipeline.stopRecording();
            }
        }
    }

    Settings {
        id: settings

        property string microscopeName: "Microscope Unnamed"
        property int streamQuality: 3
        property string videoRecordingDestination: "file://" + videoPath
        property int websocketPort: 3012
    }

    GstGLVideoItem {
        id: videoMain
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        anchors.topMargin: 60
        anchors.bottomMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 10

         objectName: "UserVideo"
         width: parent.width
         height: parent.height - 50

         PixmapViewer {
             id: annotationsUser
         }
    }
}