import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: userMenu

    property bool recording: false

    Row {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 10

        Button {
            id: recordButton
            text: "Start Recording"
            leftPadding: 30
            rightPadding: 30

            onClicked: {
                if(pipeline.isStopped) {
                    return;
                }

                recordButton.state = recordButton.state == 'recording' ? "" : "recording";
                userMenu.recording = recordButton.state == 'recording';
            }

            states: [
                State {
                    name: ""
                    PropertyChanges { target: recordButton; }
                },
                State {
                    name: "recording"
                    PropertyChanges { target: recordButton; background.color: "#c0392b"; text: "Stop Recording";}
                }
            ]
        }

        Text {
            text: "  "
        }

        Button {
            id: connectToMicroscopeButton
            text: "Connect to Microscope"

            onClicked: {
                pipeline.play();
                pipeline.isStopped = false;

                connectToMicroscopeButton.visible = false;
            }
        }
    }

    // uncomment when zoom feature is implemented
    /*Row {
        id: zoomRow
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10

        property int factor: 0

        Label {
            anchors.verticalCenter: parent.verticalCenter

            text: "Zoom: "
            font.pixelSize: 16
            font.bold: true
        }

        Text {
            id: zoomText

            anchors.verticalCenter: parent.verticalCenter

            text: {
                let steps = [1, 2, 4, 20];

                return String(steps[zoomRow.factor]) + ".0x";
            }

            font.pixelSize: 16
        }

        Text {
            text: "  "
        }

        Button {
            id: testButton
            icon.name: "Zoom Out"
            icon.source: "qrc:/Static/Buttons/ZoomOut.png"
            icon.width: 24
            icon.height: 24

            onClicked: {
                zoomRow.factor = zoomRow.factor <= 0 ? 0 : zoomRow.factor - 1;
            }
        }

        Text {
            text: "  "
        }

        Button {
            icon.name: "Zoom In"
            icon.source: "qrc:/Static/Buttons/ZoomIn.png"
            icon.width: 24
            icon.height: 24

            onClicked: {
                zoomRow.factor = zoomRow.factor >= 3 ? 3 : zoomRow.factor + 1;
            }
        }
    }*/
}