import QtQuick 2.12
import de.aachen.utilities 1.0

PixmapViewer {
    id: annotationOverlay

    width: 4 / 3 > parent.width / parent.height ? parent.width : (4 / 3) * parent.height
    height: 4 / 3 > parent.width / parent.height ? (3 / 4) * parent.width : parent.height
    anchors.centerIn: parent
}
