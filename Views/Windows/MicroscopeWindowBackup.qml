import QtQuick 2.12
import QtQuick.Window 2.12

import org.freedesktop.gstreamer.GLVideoItem 1.0

import "../Components"

Window {
        visible: false

        width: 1280
        height: 480
        title: "ICARAS"

        property alias videoLeft: videoLeft
        property alias videoRight: videoRight
        property alias annotationsLeft: annotationsLeft
        property alias annotationsRight: annotationsRight

        Row {
            anchors.fill: parent

            GstGLVideoItem {
                id: videoLeft
                width: parent.width / 2
                height: parent.height

                AnnotationOverlay {
                    id: annotationsLeft
                }
            }

            GstGLVideoItem {
                id: videoRight
                width: parent.width / 2
                height: parent.height

                AnnotationOverlay {
                    id: annotationsRight
                }
            }
        }
    }