import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

import de.aachen.utilities 1.0

Window {
    id: qrcodeWindow

    visible: true
    title: "QR Code Connect"

    minimumWidth: 500
    minimumHeight: 500
    property string address: ""

    onAddressChanged: qrCode.setString("ws://" + address + ":" + settings.websocketPort)

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        QrCodeViewer {
            id: qrCode
            height: 300
            width: 300
            Layout.alignment: Qt.AlignCenter
         }

        Text {
            text: "IP: " + qrcodeWindow.address
            Layout.alignment: Qt.AlignCenter
        }

        Text {
            text: "Port: " + settings.websocketPort
            Layout.alignment: Qt.AlignCenter
            Layout.preferredHeight: 50
        }
    }
}