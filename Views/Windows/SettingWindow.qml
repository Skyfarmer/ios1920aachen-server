import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.0

Window {
    id: settingWindow

    visible: true
    title: "Settings"

    minimumWidth: 600
    minimumHeight: 600

    function showError(message) {
        console.log("error:", message)

        let component = Qt.createComponent("Alert.qml");

        component.createObject(settingWindow, {
            text: message,
        });
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        anchors.margins: 20
        spacing: 40

        GroupBox {
            title: "Microscope Name"
            Layout.fillWidth: true

            TextInput {
                id: microscopeNameInput
                anchors.fill: parent
                text: settings.microscopeName
            }
        }

        GroupBox {
            title: "Video Destination"
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                Button {
                    text: "Select Folder"

                    leftPadding: 30
                    rightPadding: 30

                    onClicked: fileDialog.visible = true;

                    FileDialog {
                        id: fileDialog
                        selectFolder: true
                        title: "Please choose a folder"
                        folder: settings.videoRecordingDestination || shortcuts.home
                        visible: false

                        onAccepted: videoDestinationInput.text = fileDialog.folder
                    }
                }

                Text {
                    id: videoDestinationInput
                    text: settings.videoRecordingDestination
                }
            }
        }

        GroupBox {
            title: "Stream Quality"
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                ButtonGroup {
                    id: streamQualityInput
                    buttons: qualityRadios.children

                    property int selectedQuality: settings.streamQuality

                    onClicked: {
                        let qualityIDs = {
                            "Super High": 5,
                            "High": 4,
                            "Normal": 3,
                            "Low": 2,
                            "Super Low": 1
                        };

                        selectedQuality = qualityIDs[button.text]
                    }
                }

                RowLayout {
                    id: qualityRadios

                    RadioButton {
                        text: "Super High"
                        ButtonGroup.group: streamQualityInput
                        checked: settings.streamQuality == 5
                    }

                    RadioButton {
                        text: "High"
                        ButtonGroup.group: streamQualityInput
                        checked: settings.streamQuality == 4
                    }

                    RadioButton {
                        text: "Normal"
                        ButtonGroup.group: streamQualityInput
                        checked: settings.streamQuality == 3
                    }

                    RadioButton {
                        text: "Low"
                        ButtonGroup.group: streamQualityInput
                        checked: settings.streamQuality == 2
                    }

                    RadioButton {
                        text: "Super Low"
                        ButtonGroup.group: streamQualityInput
                        checked: settings.streamQuality == 1
                    }
                }

            }
        }

        Button {
            text: "Save Changes"

            leftPadding: 30
            rightPadding: 30

            onClicked: {
                if(typeof microscopeNameInput.text !== "string" || microscopeNameInput.text.length < 1) {
                    return settingWindow.showError("The microscope name should be at least one character long");
                }

                if(typeof videoDestinationInput.text !== "string" || videoDestinationInput.text.length < 5) {
                    return settingWindow.showError("Please select a folder where the recordings are saved");
                }

                settings.microscopeName = microscopeNameInput.text
                settings.videoRecordingDestination = videoDestinationInput.text;
                settings.streamQuality = streamQualityInput.selectedQuality;

                wsServer.setServerName(microscopeNameInput.text);

                settingWindow.close();
            }
        }
    }
}