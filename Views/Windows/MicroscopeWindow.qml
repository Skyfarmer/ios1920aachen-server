import QtQuick 2.12
import QtQuick.Window 2.12

import org.freedesktop.gstreamer.GLVideoItem 1.0

import "../Components"

Window {
        visible: false

        width: 1280
        height: 480
        title: "ICARAS"

        property alias videoLeft: videoLeft
        property alias videoRight: videoRight
        property alias annotationsLeft: annotationsLeft
        property alias annotationsRight: annotationsRight

        Rectangle {
            id: videoSpacer
            width: (parent.width - videoLeft.width * 2) / 2
            height: videoLeft.height
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.verticalCenter: parent
        }

        GstGLVideoItem {
            id: videoLeft
            width: 4 / 3 > parent.width / 2 / parent.height ? parent.width / 2 : (4 / 3) * parent.height
            height: 4 / 3 > parent.width / 2 / parent.height ? (3 / 4) * parent.width / 2 : parent.height
            anchors.left: videoSpacer.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.verticalCenter: parent

            AnnotationOverlay {
                id: annotationsLeft
            }
        }

        GstGLVideoItem {
            id: videoRight
            width: 4 / 3 > parent.width / 2 / parent.height ? parent.width / 2 : (4 / 3) * parent.height
            height: 4 / 3 > parent.width / 2 / parent.height ? (3 / 4) * parent.width / 2 : parent.height
            anchors.left: videoLeft.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.verticalCenter: parent

            AnnotationOverlay {
                id: annotationsRight
            }
        }
    }