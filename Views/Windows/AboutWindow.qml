import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Window {
    id: aboutWindow

    minimumWidth: 500
    minimumHeight: 700

    visible: true
    title: "About"

    ColumnLayout {
        anchors.fill: parent

        Image {
            source: "qrc:/Static/Logo/ICARAS.png"
            Layout.alignment: Qt.AlignCenter
             Layout.topMargin: 15
        }

        Image {
            source: "qrc:/Static/Logo/TUM.png"
            Layout.alignment: Qt.AlignCenter
            Layout.bottomMargin: 15
        }

        Image {
            source: "qrc:/Static/Logo/RWTH.png"
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 15
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 15

            Text {
                text: "Developed By"
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 8

            Text {
                text: "Simon Himmelbauer"
            }

            Text {
                text: " - "
            }

            Text {
                text: "Fabian Emilius"
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 8

            Text {
                text: "Emir Besic"
            }

            Text {
                text: " - "
            }

            Text {
                text: "Smail Smajlović"
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter
            Layout.margins: 8
            Layout.bottomMargin: 25

            Text {
                text: "Leonard Scheidemantel"
            }

            Text {
                text: " - "
            }

            Text {
                text: "Tanja Baklanova"
            }
        }

    }

}