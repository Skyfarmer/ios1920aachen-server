//
// Created by shimmelbauer on 1/23/20.
//

#include "PixmapViewer.h"
#include "../Global.h"

#include <QPainter>
using namespace Utilities;

PixmapViewer::PixmapViewer(QQuickItem *parent)
    : QQuickPaintedItem(parent), m_pixmap(nullptr)
{
}

void PixmapViewer::setPixmap(const std::shared_ptr<QPixmap> &pixmap)
{
    m_pixmap = pixmap;
    update();
}

void PixmapViewer::paint(QPainter *painter)
{
    if (m_pixmap != nullptr) {
        painter->drawPixmap(0, 0, width(), height(), *m_pixmap);
    }
}
