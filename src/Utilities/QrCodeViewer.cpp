//
// Created by developer on 2/5/20.
//

#include "QrCodeViewer.h"
#include <QQuickItem>

#include "../External/QrCode/QrCode.hpp"
#include "../Global.h"
using namespace Utilities;
using namespace qrcodegen;

Utilities::QrCodeViewer::QrCodeViewer(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{

}

void QrCodeViewer::setString(const QString &str)
{
    qDebug(Multiviewer) << "Updating QR-Code to" << str;
    auto qr0 = QrCode::encodeText(str.toStdString().c_str(), QrCode::Ecc::MEDIUM);
    QByteArray data(qr0.toSvgString(4).c_str());
    if (!m_qrcode.load(data)) {
        qCritical(Multiviewer) << "Could not pass QR-SVG";
    }
    update();
}

void QrCodeViewer::paint(QPainter *painter)
{
    m_qrcode.render(painter);
}
