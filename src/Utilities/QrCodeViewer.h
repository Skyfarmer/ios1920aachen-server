//
// Created by developer on 2/5/20.
//

#ifndef QRCODEVIEWER_H
#define QRCODEVIEWER_H

#include <QQuickPaintedItem>
#include <QSvgRenderer>

namespace Utilities
{
class QrCodeViewer: public QQuickPaintedItem
{
Q_OBJECT
public:
    explicit QrCodeViewer(QQuickItem *parent = nullptr);

    Q_INVOKABLE void setString(const QString &str);
protected:
    void paint(QPainter *painter) override;

private:
    QSvgRenderer m_qrcode;
};
}

#endif //QRCODEVIEWER_H
