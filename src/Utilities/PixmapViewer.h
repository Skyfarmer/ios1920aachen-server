//
// Created by shimmelbauer on 1/23/20.
//

#ifndef PIXMAPVIEWER_H
#define PIXMAPVIEWER_H

#include <QQuickPaintedItem>
#include <memory>

namespace Utilities
{

class PixmapViewer: public QQuickPaintedItem
{
Q_OBJECT
public:
    explicit PixmapViewer(QQuickItem *parent = nullptr);
    void setPixmap(const std::shared_ptr<QPixmap> &pixmap);

protected:
    void paint(QPainter *painter) override;

private:
    std::shared_ptr<QPixmap> m_pixmap;
};
}


#endif //PIXMAPVIEWER_H
