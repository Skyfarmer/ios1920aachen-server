//
// Created by shimmelbauer on 1/23/20.
//

#ifndef GLOBAL_H
#define GLOBAL_H

#include <QLoggingCategory>
Q_DECLARE_LOGGING_CATEGORY(Multiviewer)

#define ASSERT_MSG(cond, message) if(!(cond)) { \
    qCritical(Multiviewer) << message; \
    return; \
}

#endif //GLOBAL_H
