#include <QGuiApplication>
#include <QRunnable>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickWindow>
#include <gst/gst.h>
#include <Magick++.h>
#include <iostream>

#include "streamprotocol.pb.h"
#include "WebSocket/WebSocketServer.h"
#include "Utilities/PixmapViewer.h"
#include "Utilities/QrCodeViewer.h"
#include "GStreamer/Pipeline.h"
#include "Bonjour/ZCFGServiceInformation.h"
#include "Bonjour/ZCFGService.h"

int main(int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    gst_init(&argc, &argv);

    Magick::InitializeMagick(*argv);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("RWTH Aachen");
    QCoreApplication::setOrganizationDomain("rwth-aachen.de");
    QCoreApplication::setApplicationName("ICARAS");

    QGuiApplication app(argc, argv);
    QGuiApplication::setWindowIcon(QIcon("qrc:/Static/Icon.png"));

    qmlRegisterType<Utilities::PixmapViewer>("de.aachen.utilities", 1, 0, "PixmapViewer");
    qmlRegisterType<Utilities::QrCodeViewer>("de.aachen.utilities", 1, 0, "QrCodeViewer");
    qmlRegisterType<WebSocket::WebSocketServer>("de.aachen.websocket", 1, 0, "WebSocketServer");
    qmlRegisterType<GStreamer::Pipeline>("de.aachen.multimedia", 1, 0, "Pipeline");

    { //Required to register with qml
        auto qmlSink = gst_element_factory_make("qmlglsink", nullptr);
        gst_object_unref(qmlSink);
    }

    QQmlApplicationEngine engine;

    // set path variables
    QUrl appPath(QString("%1").arg(QGuiApplication::applicationDirPath()));
    engine.rootContext()->setContextProperty("appPath", appPath.resolved(QUrl(".")));

    QUrl videoPath;
    const QStringList moviesLocation = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
    if (moviesLocation.isEmpty()) {
        videoPath = appPath.resolved(QUrl("./"));
    } else {
        videoPath = QString("%1").arg(moviesLocation.first());
    }
    engine.rootContext()->setContextProperty("videoPath", videoPath);

    const QUrl url(QStringLiteral("qrc:/Views/Main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
                     {
                         if (!obj && url == objUrl)
                             QCoreApplication::exit(-1);
                     }, Qt::QueuedConnection);
    engine.load(url);

    // Startup mDNS/Bonjour/Avahi/DNS-SD
    ZCFGServiceInformation servInfo("microscope", "_icaras-service._tcp.", "local", 53567);
    ZCFGService service(argv[0], servInfo);
    int exitZCFG = service.runService();
    if (exitZCFG < 0) {
        std::cerr << "Could not start mDNS service." << std::endl;
    }

    //userWindow->setIcon(QIcon("qrc:/Static/Icon.png"));
    int ret = QGuiApplication::exec();

    // Shutdown Bonjour
    if (service.stopService() == -1) {
        std::cerr << "Could not stop mDNS service" << std::endl;
    }

    //Shutdown GStreamer
    gst_deinit();

    //Shutdown protobuf library
    google::protobuf::ShutdownProtobufLibrary();

    return ret;
}
