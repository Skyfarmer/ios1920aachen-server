//
// Created by shimmelbauer on 1/27/20.
//

#ifndef RECORDER_H
#define RECORDER_H

#include <gst/gst.h>
#include <QObject>
#include <utility>
#include <atomic>

namespace GStreamer
{
class Recorder: public QObject
{
Q_OBJECT
public:
    explicit Recorder(GstElement *pipeline, const QString &filename, QObject *parent = nullptr);
    ~Recorder() override;

public Q_SLOTS:
    void unlink();
    void remove();

Q_SIGNALS:
    void removed();

private:
    std::atomic_bool m_integrated;

    std::pair<GstPad *, GstPad *> m_teePad;
    GstElement *m_pipeline;

    std::pair<GstElement *, GstElement *> m_tee;
    std::pair<GstElement *, GstElement *> m_queue;
    std::pair<GstElement *, GstElement *> m_converter;
    std::pair<GstElement *, GstElement *> m_encoder;
    std::pair<GstElement *, GstElement *> m_h265parser;
    GstElement *m_muxer;
    GstElement *m_filesink;
};
}

#endif //RECORDER_H
