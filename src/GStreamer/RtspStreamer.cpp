//
// Created by microscope on 19.02.20.
//


#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>

#include <utility>

#include "RtspStreamer.h"
#include "../Global.h"
using namespace GStreamer;

RtspStreamer::RtspStreamer(QString address, quint32 port, QObject *parent)
    : QObject(parent), m_address(std::move(address)), m_port(port), m_streaming(false), m_server_id{}
{

}

RtspStreamer::~RtspStreamer()
{
    if (m_streaming) {
        stopStreaming();
    }
}

QString RtspStreamer::generateRtspUrl() const
{
    return QString().append("rtsp://").append(m_address)
        .append(":").append(QString::number(m_port)).append("/camera");
}

void RtspStreamer::startStreaming()
{
    ASSERT_MSG(!m_streaming, "RTSP-Server is already running")

    qDebug(Multiviewer) << "Starting RTSP-Server on" << generateRtspUrl();
    auto server = gst_rtsp_server_new();
    g_object_set(server, "service", std::to_string(m_port).c_str(), nullptr);

    auto mounts = gst_rtsp_server_get_mount_points(server);
    auto factory = gst_rtsp_media_factory_new();
    gst_rtsp_media_factory_set_launch(factory, "galaxysrc index=0 ! "
                                               "queue ! "
                                               "bayer2rgb ! "
                                               "videoscale ! video/x-raw,width=1280,height=1024 ! "
                                               "videoconvert ! "
                                               "nvh265enc ! "
                                               "rtph265pay name=pay0");
    gst_rtsp_media_factory_set_shared(factory, true);
    gst_rtsp_mount_points_add_factory(mounts, "/camera", factory);
    g_object_unref(mounts);

    m_server_id = gst_rtsp_server_attach(server, nullptr);
    m_streaming = true;
}

void RtspStreamer::stopStreaming()
{
    ASSERT_MSG(m_streaming, "RTSP-server is not running")

    qDebug(Multiviewer) << "Stopping RTSP-server";
    g_source_remove(m_server_id);
}