//
// Created by microscope on 19.02.20.
//

#ifndef RTSPSTREAMER_H
#define RTSPSTREAMER_H

#include <QObject>

namespace GStreamer
{
class RtspStreamer: public QObject
{
Q_OBJECT
public:
    explicit RtspStreamer(QString address, quint32 port, QObject *parent = nullptr);
    ~RtspStreamer() override;

    [[nodiscard]] QString generateRtspUrl() const;

public Q_SLOTS:
    void startStreaming();
    void stopStreaming();

private:
    const QString m_address;
    const quint32 m_port;
    bool m_streaming;
    unsigned int m_server_id;
};
}

#endif //RTSPSTREAMER_H
