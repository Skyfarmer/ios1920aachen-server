//
// Created by shimmelbauer on 1/27/20.
//

#include "Recorder.h"
#include "../Global.h"
using namespace GStreamer;

#define UNREF_PAIR(p) gst_object_unref(p.first); gst_object_unref(p.second);

Recorder::Recorder(GstElement *pipeline, const QString &filename, QObject *parent)
    : QObject(parent),
      m_integrated(false),
      m_pipeline(GST_ELEMENT(gst_object_ref(pipeline))),
      m_muxer(nullptr),
      m_filesink(nullptr)
{
    qDebug(Multiviewer) << "Enabling file recording on pipeline";
    auto queueLeft = gst_element_factory_make("queue", nullptr);
    auto queueRight = gst_element_factory_make("queue", nullptr);
    auto converterLeft = gst_element_factory_make("videoconvert", nullptr);
    auto converterRight = gst_element_factory_make("videoconvert", nullptr);
    auto encoderLeft = gst_element_factory_make("nvh265enc", nullptr);
    auto encoderRight = gst_element_factory_make("nvh265enc", nullptr);
    auto h265parserLeft = gst_element_factory_make("h265parse", nullptr);
    auto h265parserRight = gst_element_factory_make("h265parse", nullptr);
    auto muxer = gst_element_factory_make("qtmux", nullptr);
    auto filesink = gst_element_factory_make("filesink", nullptr);

    auto teeLeft = gst_bin_get_by_name(GST_BIN(m_pipeline), "teeLeft");
    auto teeRight = gst_bin_get_by_name(GST_BIN(m_pipeline), "teeRight");
    g_assert(
        queueLeft && queueRight && converterLeft && converterRight && encoderLeft && encoderRight && h265parserLeft
            && h265parserRight && muxer && filesink
            && teeLeft && teeRight);

    //Request tee pad
    auto teePadTemplate = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(teeLeft), "src_%u");
    auto teePadLeft = gst_element_request_pad(teeLeft, teePadTemplate, nullptr, nullptr);
    teePadTemplate = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(teeRight), "src_%u");
    auto teePadRight = gst_element_request_pad(teeRight, teePadTemplate, nullptr, nullptr);

    g_object_set(filesink, "location", filename.toStdString().c_str(), nullptr);

    gst_bin_add_many(GST_BIN(m_pipeline),
                     queueLeft,
                     queueRight,
                     converterLeft,
                     converterRight,
                     encoderLeft,
                     encoderRight,
                     h265parserLeft,
                     h265parserRight,
                     muxer,
                     filesink,
                     nullptr);
    gst_element_link_many(queueLeft, converterLeft, encoderLeft, h265parserLeft, muxer, filesink, nullptr);
    gst_element_link_many(queueRight, converterRight, encoderRight, h265parserRight, nullptr);

    gst_element_sync_state_with_parent(queueLeft);
    gst_element_sync_state_with_parent(queueRight);
    gst_element_sync_state_with_parent(converterLeft);
    gst_element_sync_state_with_parent(converterRight);
    gst_element_sync_state_with_parent(encoderLeft);
    gst_element_sync_state_with_parent(encoderRight);
    gst_element_sync_state_with_parent(h265parserLeft);
    gst_element_sync_state_with_parent(h265parserRight);
    gst_element_sync_state_with_parent(muxer);
    gst_element_sync_state_with_parent(filesink);

    auto sinkPad = gst_element_get_static_pad(queueLeft, "sink");
    gst_pad_link(teePadLeft, sinkPad);
    gst_object_unref(sinkPad);

    sinkPad = gst_element_get_static_pad(queueRight, "sink");
    gst_pad_link(teePadRight, sinkPad);
    gst_object_unref(sinkPad);

    //Request muxer pad
    auto muxPadTemplate = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(muxer), "video_%u");
    auto muxerPadRight = gst_element_request_pad(muxer, muxPadTemplate, nullptr, nullptr);
    auto srcPad = gst_element_get_static_pad(h265parserRight, "src");
    gst_pad_link(srcPad, muxerPadRight);
    gst_object_unref(srcPad);

    m_teePad.first = GST_PAD(gst_object_ref(teePadLeft));
    m_teePad.second = GST_PAD(gst_object_ref(teePadRight));
    m_tee.first = GST_ELEMENT(gst_object_ref(teeLeft));
    m_tee.second = GST_ELEMENT(gst_object_ref(teeRight));
    m_queue.first = GST_ELEMENT(gst_object_ref(queueLeft));
    m_queue.second = GST_ELEMENT(gst_object_ref(queueRight));
    m_converter.first = GST_ELEMENT(gst_object_ref(converterLeft));
    m_converter.second = GST_ELEMENT(gst_object_ref(converterRight));
    m_encoder.first = GST_ELEMENT(gst_object_ref(encoderLeft));
    m_encoder.second = GST_ELEMENT(gst_object_ref(encoderRight));
    m_h265parser.first = GST_ELEMENT(gst_object_ref(h265parserLeft));
    m_h265parser.second = GST_ELEMENT(gst_object_ref(h265parserRight));
    m_muxer = GST_ELEMENT(gst_object_ref(muxer));
    m_filesink = GST_ELEMENT(gst_object_ref(filesink));

    m_integrated = true;
}

Recorder::~Recorder()
{
    if (m_integrated) {
        qWarning(Multiviewer) << "Recorder has not been removed before destruction";
    }

    UNREF_PAIR(m_teePad)
    gst_object_unref(m_pipeline);
    UNREF_PAIR(m_tee)
    UNREF_PAIR(m_queue)
    UNREF_PAIR(m_converter)
    UNREF_PAIR(m_encoder)
    UNREF_PAIR(m_h265parser)
    gst_object_unref(m_muxer);
    gst_object_unref(m_filesink);
}

void Recorder::unlink()
{
    ASSERT_MSG(m_integrated, "Recorder not integrated into pipeline")

    auto callback = [](GstPad *pad, GstPadProbeInfo *info, gpointer user_data)
    {
        qDebug(Multiviewer) << "Unlinking Recorder";
        auto recorder = ((Recorder *) user_data);

        auto sinkpad = gst_element_get_static_pad(recorder->m_queue.first, "sink");
        gst_pad_unlink(recorder->m_teePad.first, sinkpad);
        gst_pad_send_event(sinkpad, gst_event_new_eos());
        gst_object_unref(sinkpad);


        sinkpad = gst_element_get_static_pad(recorder->m_queue.second, "sink");
        gst_pad_unlink(recorder->m_teePad.second, sinkpad);
        gst_pad_send_event(sinkpad, gst_event_new_eos());
        gst_object_unref(sinkpad);


        return GST_PAD_PROBE_REMOVE;
    };
    auto free_callback = [](gpointer data)
    {

    };

    gst_pad_add_probe(m_teePad.first, GST_PAD_PROBE_TYPE_IDLE, callback, this, nullptr);
}

void Recorder::remove()
{
    ASSERT_MSG(m_integrated, "Recorder not integrated into pipeline")

    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_queue.first);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_queue.second);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_converter.first);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_converter.second);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_encoder.first);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_encoder.second);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_h265parser.first);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_h265parser.second);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_muxer);
    gst_bin_remove(GST_BIN(this->m_pipeline), this->m_filesink);

    gst_element_set_state(this->m_queue.first, GST_STATE_NULL);
    gst_element_set_state(this->m_queue.second, GST_STATE_NULL);
    gst_element_set_state(this->m_converter.first, GST_STATE_NULL);
    gst_element_set_state(this->m_converter.second, GST_STATE_NULL);
    gst_element_set_state(this->m_encoder.first, GST_STATE_NULL);
    gst_element_set_state(this->m_encoder.second, GST_STATE_NULL);
    gst_element_set_state(this->m_h265parser.first, GST_STATE_NULL);
    gst_element_set_state(this->m_h265parser.second, GST_STATE_NULL);
    gst_element_set_state(this->m_muxer, GST_STATE_NULL);
    gst_element_set_state(this->m_filesink, GST_STATE_NULL);

    gst_element_release_request_pad(this->m_tee.first, this->m_teePad.first);
    gst_element_release_request_pad(this->m_tee.second, this->m_teePad.second);

    m_integrated = false;
    emit removed();

    qDebug(Multiviewer) << "Recording stopped";
}
