//
// Created by shimmelbauer on 24.01.20.
//

#ifndef PIPELINE_H
#define PIPELINE_H

#include <QObject>
#include <gst/gst.h>
#include <optional>
#include <QHostAddress>
#include <QThread>
#include <memory>

QT_FORWARD_DECLARE_CLASS(QQuickItem)

namespace GStreamer
{

QT_FORWARD_DECLARE_CLASS(RtspStreamer)
QT_FORWARD_DECLARE_CLASS(Recorder)

class Pipeline: public QObject
{
Q_OBJECT
public:
    enum View
    {
        Main,
        Left,
        Right
    };
    Q_ENUM(View)

    explicit Pipeline(QObject *parent = nullptr);
    ~Pipeline() override;

Q_SIGNALS:
    //Internal signals
    void _eos();

    //Public signals
    void stopped();
    void streamStarted(QString rtsp);

public Q_SLOTS:
    void setQmlSink(QQuickItem *sink, GStreamer::Pipeline::View view);
    void play();
    void stop();
    void startRecording(const QString &filename);
    void stopRecording();
    void startStreaming(const QString &address, quint32 port);
    void stopStreaming();

private:
    GstElement *m_pipeline;
    RtspStreamer *m_rtsp;
    Recorder *m_recorder;

    static GstElement *createBasePipeline();
    static void addQmlSink(GstElement *pipeline, GstElement *link, const std::string &sink_name);
    static std::optional<GstState> getState(GstElement *element);

private Q_SLOTS:
    void handleEos();
};
}


#endif //PIPELINE_H
