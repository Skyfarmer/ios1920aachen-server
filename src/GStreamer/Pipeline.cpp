//
// Created by shimmelbauer on 24.01.20.
//

#include "Pipeline.h"
#include "../Global.h"
#include "RtspStreamer.h"
#include "Recorder.h"

#include <gst/gst.h>
#include <QtGui/QGuiApplication>
#include <memory>
using namespace GStreamer;

Pipeline::Pipeline(QObject *parent)
    : QObject(parent),
      m_pipeline(createBasePipeline()),
      m_rtsp(nullptr),
      m_recorder(nullptr)
{
    if (!gst_is_initialized()) {
        gst_init(nullptr, nullptr);
    }
    auto callback = [](GstBus *bus, GstMessage *message, gpointer user_data)
    {
        auto pipeline = (Pipeline *) user_data;
        switch (GST_MESSAGE_TYPE(message)) {
            case GST_MESSAGE_EOS:
                qDebug(Multiviewer) << "Received EOS";
                emit pipeline->_eos();
                break;
            case GST_MESSAGE_ELEMENT: {
                const auto s = gst_message_get_structure(message);

                if (gst_structure_has_name(s, "GstBinForwarded")) {
                    GstMessage *forward_msg = nullptr;

                    gst_structure_get(s, "message", GST_TYPE_MESSAGE, &forward_msg, nullptr);
                    if (GST_MESSAGE_TYPE (forward_msg) == GST_MESSAGE_EOS
                        && GST_OBJECT_NAME (GST_MESSAGE_SRC(forward_msg)) == "filesink0") {
                        qDebug(Multiviewer) << "Received EOS from filesink, stopping recording...";
                        pipeline->m_recorder->remove();
                    }
                    gst_message_unref(forward_msg);
                }
                break;
            }
            default:
                return GST_BUS_PASS;
        }

        return GST_BUS_DROP;
    };
    auto bus = gst_element_get_bus(m_pipeline);
    gst_bus_set_sync_handler(bus, callback, this, nullptr);
    gst_object_unref(bus);

    connect(this, &Pipeline::_eos, this, &Pipeline::handleEos);
}

Pipeline::~Pipeline()
{
    stop();
    gst_object_unref(m_pipeline);
}

void Pipeline::setQmlSink(QQuickItem *sink, View view)
{
    qDebug(Multiviewer) << "Setting QQuickItem to qmlsink";
    GstElement *qmlSink = nullptr;
    switch (view) {
        case Main:
            qmlSink = gst_bin_get_by_name(GST_BIN(m_pipeline), "viewMain");
            break;
        case Left:
            qmlSink = gst_bin_get_by_name(GST_BIN(m_pipeline), "viewLeft");
            break;
        case Right:
            qmlSink = gst_bin_get_by_name(GST_BIN(m_pipeline), "viewRight");
            break;
    }
    g_assert(qmlSink);
    g_object_set(qmlSink, "widget", sink, nullptr);
}

void Pipeline::play()
{
    ASSERT_MSG(getState(m_pipeline).value() != GST_STATE_PLAYING, "Pipeline is already playing")

    qDebug(Multiviewer) << "Setting pipeline to PLAY";
    gst_element_set_state(m_pipeline, GST_STATE_PLAYING);
    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(m_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "playing");

    //startStreaming();
}

void Pipeline::stop()
{
    ASSERT_MSG(getState(m_pipeline).value() != GST_STATE_NULL,
               "Pipeline is already stopped")

    qDebug(Multiviewer) << "Sending EOS to properly shutdown pipeline";
    gst_element_send_event(m_pipeline, gst_event_new_eos());

}

void Pipeline::startRecording(const QString &filename)
{
    ASSERT_MSG(getState(m_pipeline).value() == GST_STATE_PLAYING,
               "Recording cannot be enabled, pipeline is not playing")
    ASSERT_MSG(m_recorder == nullptr, "Recording already activated")

    m_recorder = new Recorder(m_pipeline, filename, this);
    connect(m_recorder, &Recorder::removed, this, [this]()
    {
        m_recorder->deleteLater();
        m_recorder = nullptr;
    });
    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(m_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "recording-started");
}

void Pipeline::stopRecording()
{
    ASSERT_MSG(m_recorder != nullptr, "Recording not activated")

    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(m_pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "recording-stopped");
    m_recorder->unlink();
}

void Pipeline::startStreaming(const QString &address, quint32 port)
{
    ASSERT_MSG(getState(m_pipeline).value() == GST_STATE_PLAYING,
               "Streaming cannot be enabled, pipeline is not playing")
    ASSERT_MSG(m_rtsp == nullptr, "Streaming already activated")

    m_rtsp = new RtspStreamer(address, port, this);
    m_rtsp->startStreaming();
    emit streamStarted(m_rtsp->generateRtspUrl());
}

void Pipeline::stopStreaming()
{
    ASSERT_MSG(m_rtsp != nullptr, "Streaming not activated")

    m_rtsp->deleteLater();
    m_rtsp = nullptr;
}

GstElement *GStreamer::Pipeline::createBasePipeline()
{
    qDebug(Multiviewer) << "Creating pipeline";
    auto pipeline = gst_pipeline_new(nullptr);
    g_object_set(GST_BIN(pipeline), "message-forward", true, nullptr);

    auto srcLeft = gst_element_factory_make("galaxysrc", nullptr);
    auto srcRight = gst_element_factory_make("galaxysrc", nullptr);

    auto bayerLeft = gst_element_factory_make("bayer2rgb", nullptr);
    auto bayerRight = gst_element_factory_make("bayer2rgb", nullptr);

    auto teeLeft = gst_element_factory_make("tee", "teeLeft");
    auto teeRight = gst_element_factory_make("tee", "teeRight");

    g_assert(pipeline && srcLeft && srcRight && bayerLeft && bayerRight && teeLeft && teeRight);

    g_object_set(srcLeft, "index", 0, nullptr);
    g_object_set(srcRight, "index", 1, nullptr);

    gst_bin_add_many(GST_BIN(pipeline),
                     srcLeft,
                     srcRight,
                     bayerLeft, bayerRight,
                     teeLeft,
                     teeRight,
                     nullptr);
    gst_element_link_many(srcLeft, bayerLeft, teeLeft, nullptr);
    gst_element_link_many(srcRight, bayerRight, teeRight, nullptr);

    addQmlSink(pipeline, teeLeft, "viewMain");
    addQmlSink(pipeline, teeLeft, "viewLeft");
    addQmlSink(pipeline, teeRight, "viewRight");

    return GST_ELEMENT(pipeline);
}

void Pipeline::addQmlSink(GstElement *pipeline, GstElement *link, const std::string &sink_name)
{
    qDebug(Multiviewer) << "Adding QML elements named" << QString::fromStdString(sink_name) << "to pipeline";
    auto queue = gst_element_factory_make("queue", nullptr);
    auto glupload = gst_element_factory_make("glupload", nullptr);
    auto qmlSink = gst_element_factory_make("qmlglsink", sink_name.c_str());

    g_assert(queue && glupload && qmlSink);

    gst_bin_add_many(GST_BIN(pipeline), queue, glupload, qmlSink, nullptr);
    gst_element_link_many(link, queue, glupload, qmlSink, nullptr);
}

std::optional<GstState> Pipeline::getState(GstElement *element)
{
    GstState tmp_state;
    //GstState pend_state;
    GstStateChangeReturn ret = gst_element_get_state(element, &tmp_state, nullptr, GST_CLOCK_TIME_NONE);
    if (ret != GST_STATE_CHANGE_SUCCESS) {
        qWarning(Multiviewer) << "gst_element_get_state did not return successfully";
        return {};
    }

    return tmp_state;
}

void Pipeline::handleEos()
{
    qDebug(Multiviewer) << "Setting pipeline to NULL";
    gst_element_set_state(m_pipeline, GST_STATE_NULL);
    emit stopped();
}
