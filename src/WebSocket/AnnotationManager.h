//
// Created by developer on 1/31/20.
//

#ifndef ANNOTATIONMANAGER_H
#define ANNOTATIONMANAGER_H

#include <QObject>
#include <QReadWriteLock>
#include <QMap>
#include <QTime>
#include <QPixmap>

#include "streamprotocol.pb.h"

namespace Magick
{
QT_FORWARD_DECLARE_CLASS(Image)
}

namespace WebSocket
{
class AnnotationManager: public QObject
{
Q_OBJECT
public:
    explicit AnnotationManager(QObject *parent = nullptr);

    bool contains(int id);

public Q_SLOTS:
    void render();
    void clear();
    void addAnnotation(const StreamProtocol::Annotation &annotation);
    void updateAnnotation(const StreamProtocol::Annotation &annotation);
    void deleteAnnotation(int id);

Q_SIGNALS:
    void rendered(QPixmap pixmap);

private:
    void generateImage(const StreamProtocol::Annotation &annotation, int width, double aspectRatio);

    QTime m_last_render;
    QTime m_last_update;
    QReadWriteLock m_lock;
    QMap<int, std::shared_ptr<Magick::Image>> m_image_cache;
    QMap<int, std::shared_ptr<StreamProtocol::Annotation>> m_annotations;
};
}

#endif //ANNOTATIONMANAGER_H
