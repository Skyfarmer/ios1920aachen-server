//
// Created by developer on 1/31/20.
//

#include "AnnotationManager.h"
#include "../Global.h"
#include <Magick++.h>
#include <sstream>
using namespace WebSocket;

AnnotationManager::AnnotationManager(QObject *parent)
    : QObject(parent), m_last_render(QTime::currentTime()), m_last_update(QTime::currentTime())
{

}

bool AnnotationManager::contains(int id)
{
    m_lock.lockForRead();
    auto ret = m_annotations.contains(id);
    m_lock.unlock();
    return ret;
}

void AnnotationManager::render()
{
    //Check if we actually need to update
    ASSERT_MSG(m_last_update > m_last_render, "No new rendering required")

    m_lock.lockForRead();
    auto annotations = m_annotations.values();
    m_last_render = QTime::currentTime();
    m_lock.unlock();
    qDebug(Multiviewer) << "Converting annotations to PNG";
    const double aspectRatio = 4.0 / 3.0;
    auto width = 1280;

    // empty transparent output image with exact size
    Magick::Image output;
    output.size(Magick::Geometry(width, (unsigned int) 1.0 / aspectRatio * width));
    output.read("xc:rgba(0,0,0,0)");

    bool is_drawing = false;
    for (const auto &annotation : annotations) {
        if (!m_image_cache.contains(annotation->id())) {
            generateImage(*annotation, width, aspectRatio);
        }
        auto image = m_image_cache[annotation->id()];

        // Eraser needs special handling
        // Ignore all erasers before the first annotation is drawn
        if (annotation->svg().is_eraser() && is_drawing) {
            output.composite(*image, "+0+0", Magick::DstOutCompositeOp);
        }
        else {
            is_drawing = true;
            output.composite(*image, "+0+0", Magick::OverCompositeOp);
        }
    }

    output.magick("png32");

    Magick::Blob blob;
    output.write(&blob);

    QPixmap pixmap;
    if (pixmap.loadFromData(reinterpret_cast<const uchar *>(blob.data()), blob.length())) {
        qDebug(Multiviewer) << "SVG2PNG-conversion successful";
        emit rendered(pixmap);
    }
    else {
        qCritical(Multiviewer) << "Could not convert PNG to QPixmap";
    }
}

void AnnotationManager::clear()
{
    m_lock.lockForWrite();
    m_annotations.clear();
    m_image_cache.clear();
    m_last_update = QTime::currentTime();
    m_lock.unlock();
}

void AnnotationManager::addAnnotation(const StreamProtocol::Annotation &annotation)
{
    m_lock.lockForWrite();
    m_annotations[annotation.id()] = std::make_shared<StreamProtocol::Annotation>(annotation);
    m_last_update = QTime::currentTime();
    m_lock.unlock();
}

void AnnotationManager::updateAnnotation(const StreamProtocol::Annotation &annotation)
{
    m_lock.lockForWrite();
    m_annotations[annotation.id()] = std::make_shared<StreamProtocol::Annotation>(annotation);
    m_image_cache.remove(annotation.id());
    m_last_update = QTime::currentTime();
    m_lock.unlock();
}

void AnnotationManager::deleteAnnotation(int id)
{
    m_lock.lockForWrite();
    m_annotations.remove(id);
    m_image_cache.remove(id);
    m_last_update = QTime::currentTime();
    m_lock.unlock();
}

void AnnotationManager::generateImage(const StreamProtocol::Annotation &annotation, int width, double aspectRatio)
{
    //FIXME Set dynamically
    Magick::Color transparent(0x0, 0x0, 0x0, 0xFF);

    const auto &svg_data = annotation.svg();
    auto image = std::make_shared<Magick::Image>();

    image->backgroundColor(transparent);

    std::ostringstream svgStream;
    svgStream << R"(<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg viewBox=")";
    svgStream << svg_data.x() << ", " << svg_data.y() << ", " << svg_data.width() << ", " << svg_data.height();
    svgStream << R"(" version="1.1" width=")" << width << "\" height=\""
              << ((unsigned int) 1.0 / aspectRatio * width) << "\" ";
    svgStream << R"( xmlns="http://www.w3.org/2000/svg"><path fill="none" )";
    svgStream << "stroke-width=\"" << svg_data.stroke_width() << "\" stroke-opacity=\"" << svg_data.stroke_opacity()
              << "\" ";
    svgStream << "stroke=\"" << svg_data.stroke_color() << "\" d=\"" << svg_data.svg_path() << "\"/></svg>";
    std::string svgString = svgStream.str();

    Magick::Blob svgBlob(svgString.c_str(), svgString.length());
    image->read(svgBlob);

    m_lock.lockForWrite();
    m_image_cache[annotation.id()] = std::move(image);
    m_lock.unlock();
}