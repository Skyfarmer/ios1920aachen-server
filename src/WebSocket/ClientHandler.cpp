//
// Created by shimmelbauer on 1/22/20.
//

#include "ClientHandler.h"
#include "WebSocketServer.h"

#include <QTimer>
#include <QString>
#include <sstream>

#include "../Global.h"
using namespace WebSocket;

#define VERIFY_HANDSHAKE \
        if(!m_handshake) { \
            qCritical(Multiviewer) << "Handshake has not been initiated by" << m_client->peerAddress() \
            << ", aborting..."; \
        m_client->close(QWebSocketProtocol::CloseCodeBadOperation, "Handshake has not been initiated"); \
        return; \
        }

ClientHandler::ClientHandler(QWebSocket *client, QString server_name, QObject *parent)
    : QObject(parent),
      m_client(client),
      m_handshake(false),
      m_heartbeatInterval(5),
      m_serverName(server_name),
      m_heartbeatTimer(new QTimer(this))
{
    connect(m_client, &QWebSocket::binaryMessageReceived, this, &ClientHandler::processBinaryMessage);
    connect(m_client, &QWebSocket::disconnected, this, &ClientHandler::socketDisconnected);

    connect(m_heartbeatTimer, &QTimer::timeout, this, &ClientHandler::sendHeartbeat);

    qDebug(Multiviewer) << "Client with address" << m_client->peerAddress() << "connected";
}

QHostAddress ClientHandler::getClientAddress() const
{
    return m_client->peerAddress();
}

quint16 ClientHandler::getClientPort() const
{
    return m_client->peerPort();
}

void ClientHandler::sendVideoConfiguration(const StreamProtocol::VideoFeedConfiguration &config)
{
    sendMessage(config.SerializeAsString(), MessageTypes::VIDEO_FEED_CONFIGURATION);
}

void ClientHandler::sendAnnotationError(const StreamProtocol::AnnotationError &error)
{
    sendMessage(error.SerializeAsString(), MessageTypes::ANNOTATION_ERROR);
}

void ClientHandler::processBinaryMessage(const QByteArray &message)
{
    //Last byte identifies message type
    auto type = message.back();
    auto msg = message.left(message.size() - 1);
    switch (type) {
        case HANDSHAKE: {
            auto handshake = StreamProtocol::Handshake();
            handshake.ParseFromArray(msg, msg.size());
            handleHandshake(handshake);
            break;
        }
        case HEARTBEAT: {
            VERIFY_HANDSHAKE
            qCritical(Multiviewer) << "Heartbeat may not be sent by a client";
            break;
        }
        case ANNOTATION_ERROR: {
            VERIFY_HANDSHAKE
            auto annoError = StreamProtocol::AnnotationError();
            annoError.ParseFromArray(msg, msg.size());
            qCritical(Multiviewer) << "Annotation Error not yet implemented, continuing...";
            break;
        }
        case ANNOTATION: {
            VERIFY_HANDSHAKE
            auto annotation = StreamProtocol::Annotation();
            annotation.ParseFromArray(msg, msg.size());
            handleAnnotation(annotation);
            break;
        }
        case MICROSCOPE_RESOLUTION: {
            VERIFY_HANDSHAKE
            auto mcresolution = StreamProtocol::MicroscopeResolution();
            mcresolution.ParseFromArray(msg, msg.size());
            qCritical(Multiviewer) << "Microscope Resolution not yet implemented, continuing...";
            break;
        }
        case VIDEO_FEED_CONFIGURATION: {
            VERIFY_HANDSHAKE
            qCritical(Multiviewer) << "VideoFeedConfiguration may not be sent by a client";
            break;
        }
        case MICROSCOPE_ZOOM: {
            VERIFY_HANDSHAKE
            qCritical(Multiviewer) << "VideoFeedConfiguration may not be sent by a client";
            break;
        }
        case CLEAR_ALL: {
            VERIFY_HANDSHAKE
            auto clearAll = StreamProtocol::ClearAll();
            clearAll.ParseFromArray(msg, msg.size());
            emit this->clearAll();
            break;
        }
        default:
            qCritical(Multiviewer) << "Unknown type" << type;
            qDebug(Multiviewer) << "Entire message from" << m_client->peerAddress() << ":" << message;
            return;
    }
}

void ClientHandler::socketDisconnected()
{
    qDebug(Multiviewer) << "Client" << m_client->peerAddress() << "disconnected";
    m_client->deleteLater();
    emit disconnected();
}

void ClientHandler::sendHeartbeat()
{
    qDebug(Multiviewer) << "Sending heartbeat to client" << m_client->peerAddress();
    auto heartbeat = StreamProtocol::Heartbeat();
    sendMessage(heartbeat.SerializeAsString(), MessageTypes::HEARTBEAT);
}

void ClientHandler::sendMessage(const std::string &message, MessageTypes type)
{
    QByteArray data(message.c_str(), message.length());
    data.append(type);
    qDebug(Multiviewer) << "Sending message" << data << "to client" << m_client->peerAddress();
    m_client->sendBinaryMessage(data);
}

void ClientHandler::handleHandshake(const StreamProtocol::Handshake &handshake)
{
    qDebug(Multiviewer) << "Received handshake from" << QString::fromStdString(handshake.name());
    this->m_handshake = true;
    emit this->handshake(handshake.video_stream_port());

    //Return server handshake
    {
        auto server_handshake = StreamProtocol::Handshake();
        server_handshake.set_name(m_serverName.toStdString());
        server_handshake.set_heartbeat_interval(m_heartbeatInterval);
        sendMessage(server_handshake.SerializeAsString(), MessageTypes::HANDSHAKE);
        m_heartbeatTimer->start(static_cast<int>(m_heartbeatInterval * 1000));
    }
    //Return zoom factor
    {
        auto zoom_level = StreamProtocol::MicroscopeZoom();
        //TODO Return proper factor
        zoom_level.set_factor(1.0);
        sendMessage(zoom_level.SerializeAsString(), MessageTypes::MICROSCOPE_ZOOM);
    }
}

void ClientHandler::handleAnnotation(const StreamProtocol::Annotation &annotation)
{
    switch (annotation.action()) {
        case StreamProtocol::Annotation_Action_CREATE:
            emit annotationAdded(annotation);
            break;
        case StreamProtocol::Annotation_Action_UPDATE:
            emit annotationUpdated(annotation);
            break;
        case StreamProtocol::Annotation_Action_DELETE:
            emit annotationDeleted(annotation.id());
            break;
        default:
            qCritical(Multiviewer) << "Requested action has not been implemented!";
    }
}
