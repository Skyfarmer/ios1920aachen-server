//
// Created by shimmelbauer on 1/22/20.
//

#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include <QObject>
#include <QMap>
#include <QHostAddress>
#include "streamprotocol.pb.h"

QT_FORWARD_DECLARE_CLASS(QWebSocket)
QT_FORWARD_DECLARE_CLASS(QTimer)

namespace WebSocket
{
class ClientHandler: public QObject
{
Q_OBJECT
public:
    enum MessageTypes
    {
        HANDSHAKE = 1,
        HEARTBEAT = 2,
        ANNOTATION_ERROR = 3,
        ANNOTATION = 4,
        MICROSCOPE_RESOLUTION = 5,
        VIDEO_FEED_CONFIGURATION = 6,
        MICROSCOPE_ZOOM = 7,
        CLEAR_ALL = 8
    };
    Q_ENUM(MessageTypes)

    explicit ClientHandler(QWebSocket *client, QString server_name, QObject *parent = nullptr);

    [[nodiscard]] QHostAddress getClientAddress() const;
    [[nodiscard]] quint16 getClientPort() const;

public Q_SLOTS:
    void sendVideoConfiguration(const StreamProtocol::VideoFeedConfiguration &config);
    void sendAnnotationError(const StreamProtocol::AnnotationError &error);

Q_SIGNALS:
    void disconnected();
    void clearAll();
    void handshake(quint16 rtp_port);
    void annotationAdded(StreamProtocol::Annotation annotation);
    void annotationUpdated(StreamProtocol::Annotation annotation);
    void annotationDeleted(int id);

private Q_SLOTS:
    void processBinaryMessage(const QByteArray &message);
    void socketDisconnected();
    void sendHeartbeat();

private:
    QWebSocket *m_client;
    bool m_handshake;
    //Heartbeat interval in seconds (s)
    quint32 m_heartbeatInterval;
    //FIXME Remove server name
    QString m_serverName;
    QTimer *m_heartbeatTimer;

    void sendMessage(const std::string &data, MessageTypes type);

    void handleHandshake(const StreamProtocol::Handshake &handshake);
    void handleAnnotation(const StreamProtocol::Annotation &annotation);
};
}


#endif //CLIENTHANDLER_H
