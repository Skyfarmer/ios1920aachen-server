//
// Created by shimmelbauer on 1/22/20.
//

#ifndef MULTIVIEWER_WEBSOCKETSERVER_H
#define MULTIVIEWER_WEBSOCKETSERVER_H

#include <QObject>
#include <QtWebSockets/QtWebSockets>
#include <queue>

#include "streamprotocol.pb.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)
QT_FORWARD_DECLARE_CLASS(QQuickItem)
QT_FORWARD_DECLARE_CLASS(QTimer)

namespace Utilities
{
QT_FORWARD_DECLARE_CLASS(PixmapViewer)
}

namespace WebSocket
{

QT_FORWARD_DECLARE_CLASS(ClientHandler)
QT_FORWARD_DECLARE_CLASS(Annotation)
QT_FORWARD_DECLARE_CLASS(AnnotationManager)

class WebSocketServer: public QObject
{
Q_OBJECT
public:
    explicit WebSocketServer(QObject *parent = nullptr);
    explicit WebSocketServer(quint16 port, QObject *parent = nullptr);

    ~WebSocketServer() override;

    Q_INVOKABLE QList<QString> getAddresses();
public Q_SLOTS:
    void registerSurface(QQuickItem *surface);
    void sendRtspUrl(const QString &rtsp);
    void setServerName(const QString &name);

Q_SIGNALS:
    void clientConnected(QString address, quint16 port, quint16 rtp_port);
    void closed();
    void annotationsUpdated();

private Q_SLOTS:
    void onNewConnection();
    void socketDisconnected();
    void updatePresenters(const QPixmap &pixmap);
    void handleAnnotationAdded(const StreamProtocol::Annotation &annotation);
    void handleAnnotationUpdated(const StreamProtocol::Annotation &annotation);
    void handleAnnotationDeleted(int id);

private:
    QWebSocketServer *m_WebSocketServer;
    QString m_server_name;

    QList<ClientHandler *> m_clients;
    QList<Utilities::PixmapViewer *> m_surfaces;

    QThread m_render_thread;
    AnnotationManager *m_annotation_manager;
    QMap<int, QTimer *> m_timeouts;
};
}


#endif //MULTIVIEWER_WEBSOCKETSERVER_H
