//
// Created by shimmelbauer on 1/22/20.
//

#include "WebSocketServer.h"
#include <QWebSocketServer>
#include <QQuickItem>
#include <QPixmap>
#include <QTimer>
#include <sstream>
#include <algorithm>

#include "ClientHandler.h"
#include "../Utilities/PixmapViewer.h"
#include "AnnotationManager.h"
#include "../Global.h"
using namespace WebSocket;

WebSocketServer::WebSocketServer(QObject *parent)
    : WebSocketServer(3012, parent)
{

}

WebSocketServer::WebSocketServer(quint16 port, QObject *parent)
    : QObject(parent),
      m_WebSocketServer(new QWebSocketServer(QStringLiteral("Microscope Server"),
                                             QWebSocketServer::NonSecureMode,
                                             this)),
      m_annotation_manager(new AnnotationManager(this)),
      m_server_name(QString("Microscope"))
{
    if (m_WebSocketServer->listen(QHostAddress::Any, port)) {
        qInfo(Multiviewer) << "Microscope Web Server listening at" << m_WebSocketServer->serverAddress()
                           << "on port" << m_WebSocketServer->serverPort();
        connect(m_WebSocketServer, &QWebSocketServer::newConnection,
                this, &WebSocketServer::onNewConnection);
        connect(m_WebSocketServer, &QWebSocketServer::closed,
                this, &WebSocketServer::closed);
    }


    m_annotation_manager->moveToThread(&m_render_thread);
    connect(this,
            &WebSocketServer::annotationsUpdated,
            m_annotation_manager,
            &AnnotationManager::render,
            Qt::QueuedConnection);
    connect(m_annotation_manager,
            &AnnotationManager::rendered,
            this,
            &WebSocketServer::updatePresenters,
            Qt::QueuedConnection);
    m_render_thread.start();
}

WebSocketServer::~WebSocketServer()
{
    m_WebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
    m_render_thread.quit();
    m_render_thread.wait();
}

QList<QString> WebSocketServer::getAddresses()
{
    auto addresses = QNetworkInterface::allAddresses();
    QList<QString> result;
    std::for_each(addresses.begin(), addresses.end(), [&result](const QHostAddress &address)
    {
        if (!address.isLoopback()) {
            result.append(address.toString());
        }
    });

    return result;
}

void WebSocketServer::registerSurface(QQuickItem *_surface)
{
    auto surface = qobject_cast<Utilities::PixmapViewer *>(_surface);
    if (surface != nullptr) {
        qDebug(Multiviewer) << "Adding surface for annotations";
        m_surfaces << surface;
    }
}

void WebSocketServer::setServerName(const QString &name) {
    m_server_name = name;
}

void WebSocketServer::sendRtspUrl(const QString &rtsp)
{
    //TODO Send only to target client
    for (const auto &c : m_clients) {
        StreamProtocol::VideoFeedConfiguration config;
        config.set_type(StreamProtocol::VideoFeedConfiguration_Type_URI);
        config.set_source(rtsp.toStdString());
        c->sendVideoConfiguration(config);
    }
}

void WebSocketServer::onNewConnection()
{
    auto *clientHandler = new ClientHandler(m_WebSocketServer->nextPendingConnection(), m_server_name);

    connect(clientHandler, &ClientHandler::disconnected, this, &WebSocketServer::socketDisconnected);
    connect(clientHandler, &ClientHandler::annotationAdded, this, &WebSocketServer::handleAnnotationAdded);
    connect(clientHandler, &ClientHandler::annotationUpdated, this, &WebSocketServer::handleAnnotationUpdated);
    connect(clientHandler, &ClientHandler::annotationDeleted, this, &WebSocketServer::handleAnnotationDeleted);
    connect(clientHandler, &ClientHandler::handshake, [clientHandler, this](quint16 rtp_port)
    {
        //FIXME Qt has bug where it cannot distinguish between IPv4 and IPv6
        //Hack - Supports IPv4 only
        auto address = clientHandler->getClientAddress().toString();
        address = address.right(address.length() - address.lastIndexOf(":") - 1);

        emit clientConnected(address, clientHandler->getClientPort(), rtp_port);
    });
    connect(clientHandler, &ClientHandler::clearAll, [this]
    {
        qDebug(Multiviewer) << "Clearing all annotations";
        this->m_annotation_manager->clear();
        emit annotationsUpdated();
    });

    m_clients << clientHandler;
}

void WebSocketServer::socketDisconnected()
{
    auto *client = qobject_cast<ClientHandler *>(sender());
    m_clients.removeAll(client);
    client->deleteLater();
    emit closed();
}

void WebSocketServer::updatePresenters(const QPixmap &_pixmap)
{
    qDebug(Multiviewer) << "Updating presenters";
    auto pixmap = std::make_shared<QPixmap>(_pixmap);
    for (const auto &presenter : m_surfaces) {
        presenter->setPixmap(pixmap);
    }
}

void WebSocketServer::handleAnnotationAdded(const StreamProtocol::Annotation &annotation)
{
    qDebug(Multiviewer) << "Adding annotation with id" << annotation.id();
    m_annotation_manager->addAnnotation(annotation);
    //Setup timeout
    if (annotation.timeout() > 0) {
        qDebug(Multiviewer) << "Setting timeout for annotation with id" << annotation.id() << "to"
                            << annotation.timeout() << "seconds";
        auto timer = new QTimer();
        connect(timer, &QTimer::timeout, [this, annotation, timer]
        {
            qDebug(Multiviewer) << "Timeout for annotation" << annotation.id() << "triggered";
            this->m_annotation_manager->deleteAnnotation(annotation.id());
            emit annotationsUpdated();
            m_timeouts.remove(annotation.id());
            timer->deleteLater();
        });
        //Convert timeouts from s to ms
        timer->setInterval(static_cast<uint>(annotation.timeout() * 1000));
        timer->start();
        m_timeouts[annotation.id()] = timer;
    }
    emit annotationsUpdated();
}

void WebSocketServer::handleAnnotationUpdated(const StreamProtocol::Annotation &annotation)
{
    auto *client = qobject_cast<ClientHandler *>(sender());
    if (!m_annotation_manager->contains(annotation.id())) {
        qWarning(Multiviewer) << "Client requests annotation update for id"
                              << annotation.id() << "but id does not exist.";
        auto error = StreamProtocol::AnnotationError();
        error.set_type(StreamProtocol::AnnotationError_Type_ID_NOT_FOUND);
        std::ostringstream message;
        message << "ID " << annotation.id() << " does not exist";
        error.set_message(message.str());
        client->sendAnnotationError(error);
    }
    else {
        qDebug(Multiviewer) << "Updating annotation with id" << annotation.id();
        m_annotation_manager->updateAnnotation(annotation);

        if (m_timeouts.contains(annotation.id())) {
            m_timeouts[annotation.id()]->start();
        }

        emit annotationsUpdated();
    }
}

void WebSocketServer::handleAnnotationDeleted(int id)
{
    auto *client = qobject_cast<ClientHandler *>(sender());
    if (!m_annotation_manager->contains(id)) {
        qWarning(Multiviewer) << "Client requests annotation update for id"
                              << id << "but id does not exist.";
        auto error = StreamProtocol::AnnotationError();
        error.set_type(StreamProtocol::AnnotationError_Type_ID_NOT_FOUND);
        std::ostringstream message;
        message << "ID " << id << " does not exist";
        error.set_message(message.str());
        client->sendAnnotationError(error);
    }
    else {
        qDebug(Multiviewer) << "Removing annotation with id" << id;
        m_annotation_manager->deleteAnnotation(id);

        if (m_timeouts.contains(id)) {
            m_timeouts.remove(id);
        }
        emit annotationsUpdated();
    }
}