#!/bin/bash

source "/home/microscope/Documents/cerbero/build/dist/linux_x86_64/bin/gst-shell"

# avahi-publish-service microscope-1 _icaras-service._tcp. 0 &
env GST_PLUGIN_PATH_1_0=/usr/local/lib /home/microscope/Documents/icaras/cmake-build-release/multiviewer

trap 'kill $(jobs -p)' EXIT
