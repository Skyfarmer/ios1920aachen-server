#!/bin/bash

install_cmake() {
	sudo apt -y install cmake build-essential clang
}

install_avahi() {
    sudo apt install avahi-utils
}

install_cuda() {
	sudo apt -y install nvidia-cuda-dev
}

install_qt5() {
	sudo apt -y install qtbase5-dev qtdeclarative5-dev libqt5websockets5-dev libqt5svg5-dev libqt5x11extras5-dev # qt5Core, qt5Quick, qt5WebSockets, qt5SVG
}

install_protobuf() {	
	sudo apt -y install protobuf-compiler
}

install_qml_modules() {				
	sudo apt -y install qml-module-qtquick-controls2 \
		qml-module-qtquick-dialogs \
		qml-module-qt-labs-settings \
		qml-module-qt-labs-folderlistmodel \
		qml-module-qtquick-controls \
		qml-module-qtquick-layouts \
		qml-module-qtquick2 \
		qml-module-qtquick-window2
	}

install_imageMagick() {	
	sudo apt -y install graphicsmagick-libmagick-dev-compat librsvg2-dev

	out="$(identify -version)"
	if [[ "$out" == *"ImageMagick 7."* ]]; then
		echo 'ImageMagick version 7+ is already installed. Will not reinstall ImageMagick'
		return
	fi

	wget https://www.imagemagick.org/download/ImageMagick.tar.gz
	tar xf ImageMagick.tar.gz
	cd ImageMagick-*
	./configure --with-rsvg=yes
	make
	sudo make install
	sudo ldconfig

	echo 'Reload or start new shell instance to update Magick version'
	echo 'You can check the running ImageMagick version by running "identify -version"'
	echo 'For future reference: The last successful compilation version with the multiviewer application was "ImageMagick 7.0.9-21"'
	
	cd ..
}

install_rust_lang() {
	if [ -e "$HOME/.cargo/env" ]; then
		source "$HOME/.cargo/env"
		echo 'Rust already installed'
		return
	fi

	sudo apt -y install curl
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
	source "$HOME/.cargo/env"
}

build_gstreamer() {
    sudo apt -y install libgstrtspserver-1.0-dev
	if [ -e "./cerbero/build/dist/linux_x86_64/bin/gst-shell" ]; then
		echo GStreamer already built
		source "./cerbero/build/dist/linux_x86_64/bin/gst-shell"
		return	
	fi
	git clone https://github.com/GStreamer/cerbero.git
	./cerbero/cerbero-uninstalled bootstrap
	./cerbero/cerbero-uninstalled -v nvcodec -v qt5 package gstreamer-1.0
	sed -i '$ d' ./cerbero/build/dist/linux_x86_64/bin/gst-shell
	source "./cerbero/build/dist/linux_x86_64/bin/gst-shell"
}

build_gxplugin() {
	echo '==============GxPlugin=============='
	git clone https://bitbucket.ase.in.tum.de/scm/ios1920aachen/ios1920aachen-server-gxplugin.git gxplugin
	cd gxplugin/gxiapi-gst
	cargo build --release
	sudo cp target/release/*.so /usr/local/lib
	cd ../..
}

build_icaras() {
	echo '==============Icaras=============='
	git clone --recursive https://bitbucket.ase.in.tum.de/scm/ios1920aachen/ios1920aachen-server.git icaras
	cd icaras
	mkdir cmake-build-release
	cd cmake-build-release
	cmake .. -DCMAKE_BUILD_TYPE=RELEASE
	make
	sudo make install
	cd ../..
}

# Main
sudo apt update

echo '================CMake/GCC============'
install_cmake
echo '================Avahi================'
install_avahi
echo '=================qt5================='
install_qt5
echo '================CUDA================='
install_cuda
echo '==============Protobuf==============='
install_protobuf
echo '==============qml-modules============'
install_qml_modules
echo '==============Rust Lang=============='
install_rust_lang
echo "==============GStreamer=============="
build_gstreamer
echo "Build GStreamer GxPlugin"
build_gxplugin
echo '==============ImageMagick============'
install_imageMagick
echo "Build Icaras Software"
build_icaras
