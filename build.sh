#!/bin/sh
rm CMakeCache.txt
mkdir -pv cmake-build-debug
mkdir -pv cmake-build-release

echo '========================DEBUG========================'
cd cmake-build-debug
cmake .. -DCMAKE_BUILD_TYPE=DEBUG
make -j4

echo '=======================RELEASE======================='
cd ..
cd cmake-build-release
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make -j4

